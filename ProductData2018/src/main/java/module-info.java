/**
 * @author Stefan Prelle
 *
 */
module rpgframework.products2018 {
	exports de.rpgframework.products2018;
	opens de.rpgframework.products2018;

	provides org.prelle.rpgframework.products.ProductDataPlugin with de.rpgframework.products2018.ProductData2018;

	requires babylon;
	requires babylon.charrules;
	requires rpgframework.api;
}