/**
 * @author Stefan Prelle
 *
 */
module rpgframework.products2017 {
	exports de.rpgframework.products2017;
	opens de.rpgframework.products2017;

	provides org.prelle.rpgframework.products.ProductDataPlugin with de.rpgframework.products2017.ProductData2017;

	requires babylon;
	requires babylon.charrules;
	requires rpgframework.api;
}