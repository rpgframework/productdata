/**
 * @author Stefan Prelle
 *
 */
module rpgframework.products2015 {
	exports de.rpgframework.products2015;
	opens de.rpgframework.products2015;

	provides org.prelle.rpgframework.products.ProductDataPlugin with de.rpgframework.products2015.ProductData2015;

	requires babylon;
	requires babylon.charrules;
	requires rpgframework.api;
}