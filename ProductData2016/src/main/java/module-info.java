/**
 * @author Stefan Prelle
 *
 */
module rpgframework.products2016 {
	exports de.rpgframework.products2016;
	opens de.rpgframework.products2016;

	provides org.prelle.rpgframework.products.ProductDataPlugin with de.rpgframework.products2016.ProductData2016;

	requires babylon;
	requires babylon.charrules;
	requires rpgframework.api;
}