/**
 * @author Stefan Prelle
 *
 */
module rpgframework.products2014 {
	exports de.rpgframework.products2014;
	opens de.rpgframework.products2014;

	provides org.prelle.rpgframework.products.ProductDataPlugin with de.rpgframework.products2014.ProductData2014;

	requires babylon;
	requires babylon.charrules;
	requires rpgframework.api;
}