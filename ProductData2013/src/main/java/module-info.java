/**
 * @author Stefan Prelle
 *
 */
module rpgframework.products2013 {
	exports de.rpgframework.products2013;
	opens de.rpgframework.products2013;

	provides org.prelle.rpgframework.products.ProductDataPlugin with de.rpgframework.products2013.ProductData2013;

	requires babylon;
	requires babylon.charrules;
	requires rpgframework.api;
}