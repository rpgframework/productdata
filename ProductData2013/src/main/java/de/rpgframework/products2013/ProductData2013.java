/**
 *
 */
package de.rpgframework.products2013;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.PathMatcher;
import java.util.List;
import java.util.stream.Collectors;

import org.prelle.rpgframework.products.ProductDataPlugin;
import org.prelle.rpgframework.products.ProductServiceImpl;

import de.rpgframework.RPGFramework;

/**
 * @author prelle
 *
 */
public class ProductData2013 extends ProductDataPlugin {

	//-------------------------------------------------------------------
	/**
	 */
	public ProductData2013() {
		// TODO Auto-generated constructor stub
	}

	private static List<Path> getSuggestions(Path resource) throws IOException {
	    PathMatcher pm = resource.getFileSystem().getPathMatcher("glob:**/" + resource.getFileName());
	    try (var stream = Files.find(resource.getRoot(), Integer.MAX_VALUE, (p, a) -> a.isRegularFile() && pm.matches(p))) {
	      return stream.collect(Collectors.toList());
	    }
	}
//	//-------------------------------------------------------------------
//	public void initialize(RPGFramework framework, ProductServiceImpl prodServ) {
//		URL url = getClass().getResource("advstories/dieLetzteReise.xml");
//		System.out.println("--------URL  = "+url);
//		System.out.println("Reading from URL: URL = " + url);
//
//		try {
//			try (var reader = new BufferedReader(new InputStreamReader(url.openStream()))) {
//				reader.lines().forEachOrdered(System.out::println);
//			}
//			System.out.println();
//
//			URI uri = null;
//			Path path = null;
//			System.out.println("--------URI  = "+url.toURI());
//			uri = url.toURI();
//			System.out.println("--------Path = "+Path.of(url.toURI()));
//			path = Path.of(uri);
//			System.out.println("Reading from path: path = " + path);
//			try (var stream = Files.lines(path)) {
//				stream.forEachOrdered(System.out::println);
//			} catch (Exception e) {
//				System.err.println(e.toString());
//			}
//
//			System.out.println("getSuggestions() = "+getSuggestions(path));
//
////			System.out.println("Reading directory content from = " + path.getParent());
////			for (Path path2 : Files.newDirectoryStream(path.getParent(), "*.xml")) {
////				System.out.println("* dir content "+path2);
////			}
//		} catch (Exception e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		}
////		System.exit(0);
//
//		super.initialize(framework, prodServ);
//	}

}
